import { defineStore } from 'pinia'

export const useMenuStore = defineStore({
  id: 'menu',
  state: () => ({
    foods: [],
  }),
  getters: {
    all: (state) => state.foods
  },
  actions: {
    addFood(food) {
      this.foods.unshift(food);
    },
    setFoods(foods) {
      this.foods = foods;
    },
    removeFood(food) {
      this.foods = this.foods.filter(foodSelected => foodSelected.id !== food.id);
    },
    updateFood(food) {
      this.foods = this.foods.map(foodSelected => {
        if (foodSelected.id === food.id) {
          return food;
        }

        return foodSelected;
      })
    }
  }
})
