import { defineStore } from 'pinia'

export const useAuthStore = defineStore({
  id: 'auth',
  state: () => ({
    user: {},
  }),
  getters: {
    isLoggedIn: (state) => !!Object.keys(state.user).length
  },
  actions: {
    addUser(user) {
      this.user = user;
    }
  }
})
