import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import Modal from '../Modal.vue'

describe('Modal', () => {
  it('should be open', () => {
    const wrapper = mount(Modal, { propsData: { open: true, title: "my modal", content: "test", closeButtonText: "close", agreeButtonText: "agree" } })
    expect(wrapper.text()).toContain('my modal')
    expect(wrapper.text()).toContain('test')
    expect(wrapper.text()).toContain('close')
    expect(wrapper.text()).toContain('agree')
  })
})
