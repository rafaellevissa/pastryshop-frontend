import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import FormMenu from '../FormMenu.vue'

describe('FormMenu', () => {
  it('should be open', () => {
    const wrapper = mount(FormMenu, { propsData: { open: true } })
    expect(wrapper.text()).toContain('Monte aqui o seu cardápio. O que está esperando?')
  })
})
