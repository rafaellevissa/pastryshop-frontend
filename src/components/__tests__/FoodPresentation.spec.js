import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import FoodPresentation from '../FoodPresentation.vue'

describe('FoodPresentation', () => {
  it('should be open', () => {
    const wrapper = mount(FoodPresentation, {
      propsData: {
        foods: [
          { id: 1, title: 'food', price: 10, picture: '', flavor: 'flavor', description: 'description' }
        ]
      }
    })
    expect(wrapper.text()).toContain('food')
    expect(wrapper.text()).toContain(10)
    expect(wrapper.text()).toContain('flavor')
    expect(wrapper.text()).toContain('description')
  })
})
