import Vue from 'vue'
import { createPinia, PiniaVuePlugin } from 'pinia'
import { createPersistedStatePlugin } from 'pinia-plugin-persistedstate-2'
import vuetify from '@/plugins/vuetify'
import VuetifyMoney from "vuetify-money";
import VueMask from 'v-mask';

import App from './App.vue'
import router from './router'

import './assets/main.css'

const pinia = createPinia()
const installPersistedStatePlugin = createPersistedStatePlugin()

pinia.use((context) => installPersistedStatePlugin(context))
Vue.use(PiniaVuePlugin)
Vue.use(VuetifyMoney)
Vue.use(VueMask)

new Vue({
  router,
  vuetify,
  pinia,
  render: (h) => h(App)
}).$mount('#app')
