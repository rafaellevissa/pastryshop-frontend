import axios from 'axios';
const BACKEND_API = import.meta.env.VITE_BACKEND_API

const api = axios.create({
  baseURL: BACKEND_API,
  headers: {
    'Accept': 'application/json'
  }
});

api.interceptors.request.use(
  (config) => {
    try {
      const { user } = JSON.parse(localStorage.getItem('auth'));

      config.headers.Authorization = `Bearer ${user.access_token}`;

      return config;
    } catch {
      return config;
    }
  }
);

api.interceptors.response.use(
  (response) => response,
  (error) => {
    const auth = localStorage.getItem('auth');

    if (error.response?.status === 401 && auth) {
      localStorage.removeItem('auth');
      window.location.reload();
    }

    return Promise.reject(error);
  },
);

export default api;