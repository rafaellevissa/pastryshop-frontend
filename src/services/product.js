import api from './api';

export async function store(payload) {
  return api.post('/products', payload);
}

export async function update(payload) {
  const { id, picture, title: name, flavor, description, price } = payload
  return api.put(`/products/${id}`, {
    name,
    flavor,
    description,
    price
  });
}

export async function list() {
  return api.get('/products');
}

export async function destroy(food) {
  return api.delete(`/products/${food.id}`);
}

export async function findById(id) {
  return api.get(`/products/${id}`);
}