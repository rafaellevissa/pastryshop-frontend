import api from './api';

export async function login(payload) {
  return api.post('/auth/login', payload);
}

export async function signup(payload) {
  return api.post('/auth/signup', payload);
}