# PASTRYSHOP FRONTEND

This is a frontend made with `vuejs`, the technologies used can be seen down bellow:

| NAME       | VERSION |
| ---------- | ------- |
| vuejs      | 2.7.7   |
| vuetify    | 2.6.8   |

### USAGE

It was created a `Dockerfile` and `docker-compose.yml` so then the project can be up and running easily typing the folowing command:

```
docker-compose up -d --build
```

After that, you can look it running at `http://localhost:3000`.

#### USER CREDENTIALS:

```
email: client@email.com
password: password
```

### TESTS

It's been using `vitest` to run the tests, so make sure you install all the dependencies with `npm i` before running the tests:

```
npm run test:unit
```
